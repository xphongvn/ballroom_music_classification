import madmom
import time
import os
from tqdm import tqdm
import numpy as np
import librosa
from scipy.fftpack import fft
from pyAudioAnalysis import audioFeatureExtraction

tqdm.monitor_interval = 0

# Name of Song,Type of Dance,Count, BPM


## Main program ##

def extract(file_out, folder_in):
    # Open a csv and write the title
    f = open(file_out, mode = 'w')
    try:
        string_to_write = "Name of Song,Type of Dance,Count,BPM,Spectral Centroid,Zero Crossing Rate," \
                          "Chroma Frequencies,Spectral Roll-off,Spectral Bandwidth"
        for i in range(1,21):
            string_to_write += ",MFCC_" + str(i)

        f.write(string_to_write + "\n")
        # Open all wav songs and calculate the bpm and write to the csv file
        list_of_dir = os.listdir(folder_in)
        if ".DS_Store" in list_of_dir:
            list_of_dir.remove(".DS_Store")

        for dir in list_of_dir:
            folder_path = folder_in + dir
            file_list = os.listdir(folder_path)
            file_list.sort()
            for file in tqdm(file_list):
                file_path = folder_path + "/" + file
                try:
                    ###### Count extraction #####
                    proc = madmom.features.DBNDownBeatTrackingProcessor(beats_per_bar=[2, 3, 4], fps=100)
                    act = madmom.features.RNNDownBeatProcessor()(file_path)
                    count = proc(act)
                    max_count = int(count[:,1].max())
                    #print("Successfully extracted count feature")

                    ##### BPM extraction ######
                    data, sr = librosa.load(file_path)
                    tempo, beats = librosa.beat.beat_track(y=data, sr=sr)
                    #print("Successfully extracted bpm feature")


                    ##### Spectral Centroid extraction ######
                    spec_cent = librosa.feature.spectral_centroid(y=data, sr=sr)

                    ##### Zero Crossing Rate extraction ######
                    zcr = librosa.feature.zero_crossing_rate(y=data)

                    ##### Chroma Frequencies extraction ######
                    chroma_stft = librosa.feature.chroma_stft(y=data, sr=sr)

                    ##### Spectral Roll-off extraction ######
                    rolloff = librosa.feature.spectral_rolloff(y=data, sr=sr)

                    ##### Spectral Bandwidth extraction ######
                    spec_bw = librosa.feature.spectral_bandwidth(y=data, sr=sr)

                    ##### Mel-frequency cepstral coefficients (MFCC) extraction ######
                    mfcc = librosa.feature.mfcc(y=data, sr=sr)

                    ##### Write to file #######

                    string_to_write = '"'+ file + '"' + "," + dir + "," + str(max_count) + "," + str(tempo) + \
                                      "," + str(np.mean(spec_cent)) + "," + str(np.mean(zcr)) \
                                      + "," + str(np.mean(chroma_stft)) + "," + str(np.mean(rolloff)) + "," + \
                                      str(np.mean(spec_bw))

                    for e in mfcc:
                        string_to_write += ',' + str(np.mean(e))

                    f.write(string_to_write + "\n")

                except Exception as e:
                    print("error in file:",file, "\n" + str(e))
    except:
        f.close()

    finally:
        f.close()


extract(file_out='song_features_public_train_2.csv', folder_in="../public_chunks_train/")
extract(file_out='song_features_public_test_2.csv', folder_in="../public_chunks_test/")