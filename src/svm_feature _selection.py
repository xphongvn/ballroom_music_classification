import pandas as pd
from sklearn.svm import SVC
from sklearn import metrics
from sklearn.model_selection import train_test_split
import seaborn as sns
import matplotlib.pyplot as plt
from sklearn.preprocessing import MinMaxScaler
import pickle
from sklearn.feature_selection import SelectKBest
from sklearn.feature_selection import chi2
import numpy as np


# read the song_features file
fn_train = "/Users/noemievoss/ballroommusicclassify/src/song_features_advanced_train.csv"
df_train = pd.read_csv(fn_train)
# read the song_features file
fn_test = "/Users/noemievoss/ballroommusicclassify/src/song_features_advanced_test.csv"
df_test = pd.read_csv(fn_test)

# Choose 4 types of dance
df_train = df_train[df_train["Type of Dance"].isin(["chachacha", "vienneze_waltz", "waltz", "samba"])]
df_test = df_test[df_test["Type of Dance"].isin(["chachacha", "vienneze_waltz", "waltz", "samba"])]

# convert types of dance into unique integer numbers in order to classify
targets = df_train["Type of Dance"].unique()
map_to_integer = {dance:n for n, dance in enumerate(targets)}
map_to_labels = {n:dance for n, dance in enumerate(targets)}
targets_column_train = df_train["Type of Dance"].replace(map_to_integer)
targets_column_test = df_test["Type of Dance"].replace(map_to_integer)


# subset the features data
features = list(df_train.columns[2:])

# split data into train and test data
#X_train, X_test, y_train, y_test = train_test_split(df[features], targets_column, test_size=.2, random_state=100)
X_train = df_train[features]
y_train = targets_column_train
X_test = df_test[features]
y_test = targets_column_test

# scale the data (normalise the data into the range 0-1)
scaler = MinMaxScaler() # initialise the scaler
X_train = scaler.fit_transform(X_train)
X_test = scaler.transform(X_test)

# build the model
svm = SVC()
# Feature Selection
test = SelectKBest(score_func=chi2, k=10)
fit = test.fit(X_train, y_train)

# print out features selection
# summarize scores
np.set_printoptions(precision=3)
print(fit.scores_)
features_transform = fit.transform(X_train)


# # evaluate the model
# y_pred = svm.predict(X_test)
# # convert y_pred and y_test back into labels
# y_test_labels = y_test.replace(map_to_labels)
# y_pred_labels = pd.Series(y_pred).replace(map_to_labels)
#
# score = metrics.classification_report(y_test_labels, y_pred_labels)
# print(score)
#
#
# plt.figure()
# mat = metrics.confusion_matrix(y_test_labels, y_pred_labels)
# sns.heatmap(mat.T, square=True, annot=True, fmt='d', cbar=False,
#             xticklabels=targets, yticklabels=targets)
# plt.xlabel('true label')
# plt.ylabel('predicted label')
# plt.title('Result of SVM Model')
# plt.show()
#
# # Save the model
# file_pickle = "../saved_models/svm.pickle"
# with open(file_pickle, 'wb') as f:
#     pickle.dump(svm, f)