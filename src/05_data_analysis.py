import pandas as pd
from sklearn.svm import SVC
from sklearn import metrics
from sklearn.model_selection import train_test_split
import seaborn as sns
import matplotlib.pyplot as plt
from sklearn.preprocessing import StandardScaler
import pickle
import seaborn as sns

# read the song_features file
fn_train = "/Users/noemievoss/ballroommusicclassify/src/song_features_advanced_train.csv"
df_train = pd.read_csv(fn_train)
# read the song_features file
fn_test = "/Users/noemievoss/ballroommusicclassify/src/song_features_advanced_test.csv"
df_test = pd.read_csv(fn_test)

# Choose 4 types of dance
df_train = df_train[df_train["Type of Dance"].isin(["chachacha", "vienneze_waltz", "waltz", "samba"])]
df_test = df_test[df_test["Type of Dance"].isin(["chachacha", "vienneze_waltz", "waltz", "samba"])]

sns.boxplot(x= "Type of Dance", y = " BPM", data = df_train)
sns.boxplot(x= "Type of Dance", y = "Count", data = df_train)