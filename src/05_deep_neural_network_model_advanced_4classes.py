import numpy as np
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import Dropout
from keras.utils import np_utils
import pandas as pd
from sklearn import metrics
from sklearn.model_selection import train_test_split
import seaborn as sns
import matplotlib.pyplot as plt
from sklearn.preprocessing import StandardScaler
import pickle

# fix random seed for reproducibility
seed = 7
np.random.seed(seed)

# read the song_features file
fn_train = "song_features_advanced_train.csv"
df_train = pd.read_csv(fn_train)
# read the song_features file
fn_test = "/Users/noemievoss/ballroommusicclassify/src/song_features_advanced_test.csv"
df_test = pd.read_csv(fn_test)

# Choose 4 types of dance
df_train = df_train[df_train["Type of Dance"].isin(["chachacha", "vienneze_waltz", "waltz", "samba"])]
df_test = df_test[df_test["Type of Dance"].isin(["chachacha", "vienneze_waltz", "waltz", "samba"])]

# Remove features
df_train = df_train[["Name of Song", "Type of Dance", "Count", "BPM"]]
df_test = df_test[["Name of Song", "Type of Dance", "Count", "BPM"]]

# convert types of dance into unique integer numbers in order to classify
targets = df_train["Type of Dance"].unique()
map_to_integer = {dance:n for n, dance in enumerate(targets)}
map_to_labels = {n:dance for n, dance in enumerate(targets)}
targets_column_train = df_train["Type of Dance"].replace(map_to_integer)
targets_column_test = df_test["Type of Dance"].replace(map_to_integer)

# subset the features data
features = list(df_train.columns[2:])

# split data into train and test data
#X_train, X_test, y_train, y_test = train_test_split(df[features], targets_column, test_size=.2, random_state=100)
X_train = df_train[features]
y_train = targets_column_train
X_test = df_test[features]
y_test = targets_column_test

# scale the data (normalise the data into the range 0-1)
scaler = StandardScaler() # initialise the scaler
X_train = scaler.fit_transform(X_train)
X_test = scaler.transform(X_test)

# Save the scaler
file_pickle = "../saved_models/x_scaler.pickle"
with open(file_pickle, 'wb') as f:
    pickle.dump(scaler, f)

uniques, ids = np.unique(y_train, return_inverse=True)
uniques_test, ids_test = np.unique(y_test, return_inverse=True)

y_train = np_utils.to_categorical(ids, len(uniques)) # one hot encoding
y_test_oh = np_utils.to_categorical(ids_test, len(uniques_test)) # one hot encoding

# define baseline model
def baseline_model(no_of_classes):
    # create model
    model = Sequential()
    model.add(Dense(8, input_dim=len(features), kernel_initializer='normal', activation='relu'))
    model.add(Dense(16, kernel_initializer='normal', activation='relu'))
    model.add(Dense(no_of_classes, kernel_initializer='normal', activation='softmax'))
    # Compile model
    model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
    model.summary()
    return model

# build the model
no_of_classes = len(uniques)
model = baseline_model(no_of_classes)
# Fit the model
model.fit(X_train, y_train, epochs=100, batch_size=100, verbose=2, validation_data=(X_test, y_test_oh))

# evaluate the model
y_pred = model.predict(X_test)
# Convert y_pred from One-hot to integer
y_pred = uniques[y_pred.argmax(axis = 1)]


# convert y_pred and y_test back into labels
y_test_labels = pd.Series(y_test).replace(map_to_labels)
y_pred_labels = pd.Series(y_pred).replace(map_to_labels)

score = metrics.classification_report(y_test_labels, y_pred_labels)
print(score)


plt.figure()
mat = metrics.confusion_matrix(y_test_labels, y_pred_labels)
sns.heatmap(mat.T, square=True, annot=True, fmt='d', cbar=False,
            xticklabels=targets, yticklabels=targets)
plt.xlabel('True Label')
plt.ylabel('Predicted Label')
plt.title('Result of Deep Neural Network Model (2 hidden layers)')
plt.show()

# Save the model
file_model = "../saved_models/neural_network.h5"
model.save(file_model)