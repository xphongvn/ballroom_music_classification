import pandas as pd
from sklearn import metrics
from sklearn.model_selection import train_test_split
import seaborn as sns
import matplotlib.pyplot as plt

# read the song_features file
fn_train = "song_features_public_train.csv"
df_train = pd.read_csv(fn_train)
# read the song_features file
fn_test = "song_features_public_test.csv"
df_test = pd.read_csv(fn_test)

def predict(df):
    waltz = df["Count"] == 3
    label = df["Type of Dance"].isin(["Waltz", "Viennesewaltz"])
    return label, waltz


label, waltz = predict(df_train)

score = metrics.classification_report(label, waltz)
print(score)

plt.figure()
mat = metrics.confusion_matrix(label, waltz)
sns.heatmap(mat.T, square=True, annot=True, fmt='d', cbar=False,
            xticklabels=["No Waltz","Waltz"], yticklabels=["No Waltz","Waltz"])
plt.xlabel('true label')
plt.ylabel('predicted label')
plt.title('Waltz vs No Waltz')
plt.show()
