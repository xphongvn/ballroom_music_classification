import numpy as np
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import Dropout
from keras.utils import np_utils
import pandas as pd
from sklearn import metrics
from sklearn.model_selection import train_test_split
import seaborn as sns
import matplotlib.pyplot as plt

# fix random seed for reproducibility
seed = 7
np.random.seed(seed)

# read the song_features file
fn = "song_features.csv"
df = pd.read_csv(fn)

# convert types of dance into unique integer numbers in order to classify
targets = df["Type of Dance"].unique()
map_to_integer = {dance:n for n, dance in enumerate(targets)}
map_to_labels = {n:dance for n, dance in enumerate(targets)}
df["targets"] = df["Type of Dance"].replace(map_to_integer)

# subset the features data
features = list(df.columns[2:9])

# split data into train and test data
X_train, X_test, y_train, y_test = train_test_split(df[features], df["targets"], test_size=.2, random_state=100)
uniques, ids = np.unique(y_train, return_inverse=True)
y_train = np_utils.to_categorical(ids, len(uniques)) # one hot encoding

# define baseline model
def baseline_model():
    # create model
    model = Sequential()
    model.add(Dense(300, input_dim=7, kernel_initializer='normal', activation='relu'))
    model.add(Dense(250, kernel_initializer='normal', activation='relu'))
    model.add(Dense(10, kernel_initializer='normal', activation='softmax'))
    # Compile model
    model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
    model.summary()
    return model

# build the model
model = baseline_model()
# Fit the model
model.fit(X_train.values, y_train, epochs=1000, batch_size=100, verbose=2)

# evaluate the model
y_pred = model.predict(X_test)
# Convert y_pred from One-hot to integer
y_pred = uniques[y_pred.argmax(axis = 1)]


# convert y_pred and y_test back into labels
y_test_labels = pd.Series(y_test).replace(map_to_labels)
y_pred_labels = pd.Series(y_pred).replace(map_to_labels)

score = metrics.classification_report(y_test_labels, y_pred_labels)
print(score)


plt.figure()
mat = metrics.confusion_matrix(y_test_labels, y_pred_labels)
sns.heatmap(mat.T, square=True, annot=True, fmt='d', cbar=False,
            xticklabels=targets, yticklabels=targets)
plt.xlabel('true label')
plt.ylabel('predicted label')
plt.title('Result of Deep Learning (2 hidden layers)')
plt.show()