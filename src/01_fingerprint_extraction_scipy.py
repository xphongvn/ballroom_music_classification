#import matplotlib.pyplot as plt
from scipy.io import wavfile as wav
from scipy.fftpack import fft
import numpy as np
import os
from tqdm import tqdm
tqdm.monitor_interval = 0


# rate, data = wav.read('/Users/noemievoss/ballroommusicclassify/music_sample_chunks_train/chachacha/02  Maroon 5 - Makes Me Wonder.mp3_0.wav')
# data_sum = data[:,0] + data[:,1]
# fft_out = fft(data_sum)
# fft_out = np.abs(fft_out)

#
# plt.plot(fft_out)
# plt.show()
#
# plt.plot(data_sum)
# plt.show()

RANGE = [30,40,80,120,180,300]

# for i in range(len(RANGE)-1):
#     start = i
#     end = start + 1
#     start_freq = RANGE[start]
#     end_freq = RANGE[end]
#     max_range = start_freq+np.argmax(fft_out[start_freq:end_freq])
#     print(max_range)


f = open('song_fingerprint.csv', mode = 'w')
f.write("Name of Song, Type of Dance, maxfreq_30_40, maxfreq_40_80, maxfreq_80_120, maxfreq_120_180, maxfreq_180_300\n")

list_of_dir = os.listdir("../music_sample_chunks_train/")
if ".DS_Store" in list_of_dir:
    list_of_dir.remove(".DS_Store")

for dir in list_of_dir:
    folder_path = "../music_sample_chunks_train/" + dir
    file_list = os.listdir(folder_path)
    for file in tqdm(file_list):
        file_path = folder_path + "/" + file
        try:
            rate, data = wav.read(file_path)
            data_sum = data[:, 0] + data[:, 1]
            fft_out = fft(data_sum)
            fft_out = np.abs(fft_out)
            max_ranges = []
            for i in range(len(RANGE) - 1):
                start = i
                end = start + 1
                start_freq = RANGE[start]
                end_freq = RANGE[end]
                max_range = start_freq + np.argmax(fft_out[start_freq:end_freq])
                max_ranges.append(max_range)

            f.write(file + "," + dir + "," + str(max_ranges[0]) + "," + str(max_ranges[1]) + "," +
                    str(max_ranges[2]) + "," + str(max_ranges[3]) + "," + str(max_ranges[4]) + "\n")
            #print("The hashtag of " + file + " is: " + str(max_ranges[0]) + "," + str(max_ranges[1]) + "," +
            #        str(max_ranges[2]) + "," + str(max_ranges[3]) + "," + str(max_ranges[4]))
        except:
            print("error in file:",file)

f.close()