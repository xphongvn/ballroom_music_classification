import pandas as pd
from sklearn.neighbors import KNeighborsClassifier
from sklearn import metrics
from sklearn.model_selection import train_test_split
import seaborn as sns
import matplotlib.pyplot as plt
from sklearn.preprocessing import StandardScaler
from keras.callbacks import EarlyStopping
from keras.optimizers import Adam
import pickle
from tqdm import tqdm

# read the song_features file
fn_train = "song_features_pro_train.csv"
df_train = pd.read_csv(fn_train)
# read the song_features file
fn_test = "song_features_pro_test.csv"
df_test = pd.read_csv(fn_test)

df_train = df_train[df_train["Type of Dance"].isin(["chachacha", "samba", "rumba", "jive", "quickstep", "foxtrot_blues",
                                                    "tango", "paso_doble"])]
df_test = df_test[df_test["Type of Dance"].isin(["chachacha", "samba", "rumba", "jive", "quickstep", "foxtrot_blues",
                                                    "tango", "paso_doble"])]

#df_train = df_train[['Name of Song', 'Type of Dance', 'Count', 'BPM', 'mean_chroma_10', 'mean_spectral_centroid', 'mean_chroma_9']]
#df_test = df_test[['Name of Song', 'Type of Dance', 'Count', 'BPM', 'mean_chroma_10', 'mean_spectral_centroid', 'mean_chroma_9']]

mapping = {"chachacha":"group1",
           "samba":"group1",
           "rumba":"group1",
           "paso_doble":"group1",
           "jive":"group5",
           "quickstep":"group1",
           "foxtrot_blues":"group5",
           "tango":"group1"}

# convert types of dance into unique integer numbers in order to classify
df_train["Type of Dance"] = df_train["Type of Dance"].replace(mapping)
df_test["Type of Dance"] = df_test["Type of Dance"].replace(mapping)
targets = df_train["Type of Dance"].unique()
map_to_integer = {dance:n for n, dance in enumerate(targets)}
map_to_labels = {n:dance for n, dance in enumerate(targets)}
targets_column_train = df_train["Type of Dance"].replace(map_to_integer)
targets_column_test = df_test["Type of Dance"].replace(map_to_integer)

df_train[df_train["BPM"]<90] = df_train[df_train["BPM"]<90] * 2
df_test[df_test["BPM"]<90] = df_test[df_test["BPM"]<90] * 2

# subset the features data
features = list(df_train.columns[2:])

# split data into train and test data
X_train = df_train[features]
y_train = targets_column_train
X_test = df_test[features]
y_test = targets_column_test

# scale the data (normalise the data into the range 0-1)
scaler = StandardScaler() # initialise the scaler
X_train = scaler.fit_transform(X_train)
X_test = scaler.transform(X_test)

# build the model
knn = KNeighborsClassifier(n_neighbors=20)
knn.fit(X_train, y_train)

# evaluate the model
y_pred = knn.predict(X_test)
# convert y_pred and y_test back into labels
y_test_labels = y_test.replace(map_to_labels)
y_pred_labels = pd.Series(y_pred).replace(map_to_labels)

score = metrics.classification_report(y_test, y_pred)
print(score)
print("f1:", metrics.f1_score(y_test,y_pred, average="weighted"))

plt.figure()
mat = metrics.confusion_matrix(y_test, y_pred)
sns.heatmap(mat.T, square=True, annot=True, fmt='d', cbar=False,
            xticklabels=targets, yticklabels=targets)
plt.xlabel('true label')
plt.ylabel('predicted label')
plt.title('Result of KNN Model (neighbours=5)')
plt.show()

# Save the model
# file_pickle = "../saved_models/knn.pickle"
# with open(file_pickle, 'wb') as f:
#     pickle.dump(knn, f)


# Find the best k
k_max = 50
f1_scores = []
k_list = range(21,k_max)

for k in tqdm(k_list):
    # build the model
    knn = KNeighborsClassifier(n_neighbors=k)
    knn.fit(X_train, y_train)
    # evaluate the model
    y_pred = knn.predict(X_test)
    f1 = metrics.f1_score(y_test, y_pred, average="weighted")
    f1_scores.append(f1)

plt.figure()
plt.plot(k_list, f1_scores)
plt.show()

