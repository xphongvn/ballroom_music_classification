from pydub import AudioSegment
import os
from tqdm import tqdm
tqdm.monitor_interval = 0

FIXED_WINDOWS_TO_CUT = 10 *1000 # cut 10 second windows
OVERLAP = 5 *1000 # define the 5 seconds overlap

def cut(folder_name_in, folder_name_out):
    # choose a folder as an input file
    list_of_dir = os.listdir(folder_name_in)
    if ".DS_Store" in list_of_dir:
        list_of_dir.remove(".DS_Store")

    # create a folder for the output files if it doesn't exist
    check_dir = folder_name_out
    if not os.path.isdir(check_dir):
        os.mkdir(check_dir)

    # prepare all the folders in the output folder
    for dir in list_of_dir:
        check_dir = folder_name_out + dir
        if not os.path.isdir(check_dir):
            os.mkdir(check_dir)

    # for each of the input files, cut into 10 second intervals
    for dir in list_of_dir:
        folder_to_convert = folder_name_in + dir
        files_to_convert = os.listdir(folder_to_convert)

        try:
            files_to_convert.remove(".DS_Store") # try to remove the meta folder
        except:
            pass

        folder_to_save = folder_name_out + dir
        for filename in tqdm(files_to_convert):
            try:
                sound = AudioSegment.from_file(folder_to_convert + "/" + filename) # read the whole song
                # determine how many chunks you will cut the song into
                iterations = int(sound.duration_seconds*1000/(FIXED_WINDOWS_TO_CUT-OVERLAP))
                for i in range(iterations):
                    start = i * (FIXED_WINDOWS_TO_CUT-OVERLAP) # determine the start and end
                    end = start + FIXED_WINDOWS_TO_CUT
                    song_10_seconds = sound[start:end]
                    song_10_seconds.export(folder_to_save + "/" + filename + "_" + str(i) +".wav", format="wav")
            except:
                print("Error in " + filename)



cut(folder_name_in = "../music_sample_mp3/", folder_name_out = "../music_sample_chunks_train/")
cut(folder_name_in = "../music_test_mp3/", folder_name_out = "../music_sample_chunks_test/")