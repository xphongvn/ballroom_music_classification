import madmom
import time
import os
from tqdm import tqdm
import numpy as np
import librosa
from scipy.fftpack import fft
from pyAudioAnalysis import audioFeatureExtraction
from scipy import signal
from scipy.io import wavfile
import matplotlib.pyplot as plt
tqdm.monitor_interval = 0



def convert(folder_out, folder_in):
    # Open all wav songs and calculate the bpm and write to the csv file
    list_of_dir = os.listdir(folder_in)
    if ".DS_Store" in list_of_dir:
        list_of_dir.remove(".DS_Store")

    # create a folder for the output files if it doesn't exist
    check_dir = folder_out
    if not os.path.isdir(check_dir):
        os.mkdir(check_dir)

    for dir in list_of_dir:
        folder_path = folder_in + dir
        folder_path_out = folder_out + dir

        # create a folder for the output files if it doesn't exist
        check_dir = folder_path_out
        if not os.path.isdir(check_dir):
            os.mkdir(check_dir)
        file_list = os.listdir(folder_path)
        file_list.sort()
        pbar = tqdm(file_list)
        for file in pbar:
            pbar.set_description("Processing " + file)
            file_path = folder_path + "/" + file
            file_path_out = folder_path_out + "/" + file + ".png"
            try:
                # Convert them into spectrogram pictures
                data, sr = librosa.load(file_path)
                frequencies, times, spectrogram = signal.spectrogram(data, sr)
                fig = plt.figure(frameon=False)
                #fig.set_size_inches(3, 3)
                ax = plt.Axes(fig, [0., 0., 1., 1.])
                ax.set_axis_off()
                fig.add_axes(ax)
                ax.pcolormesh(times, frequencies, spectrogram, cmap='spring')
                ax.imshow(spectrogram)
                fig.savefig(file_path_out, dpi = 30, bbox_inches = "tight", pad_inches=0)
                plt.close()
            except Exception as e:
                print("There an error:", file)
                print(e)

convert(folder_out= "../spectrogram_test/", folder_in="../music_sample_chunks_test/")
convert(folder_out= "../spectrogram_train/", folder_in="../music_sample_chunks_train/")
