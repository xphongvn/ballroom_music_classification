import madmom
import time
import os
from tqdm import tqdm
import numpy as np
import librosa
from scipy.fftpack import fft
from pyAudioAnalysis import audioFeatureExtraction
import sys

tqdm.monitor_interval = 0
RANGE = [30,40,80,120,180,300] # frequency range for fingerprint

# Name of Song,Type of Dance,Count, BPM, maxfreq_30_40, maxfreq_40_80, maxfreq_80_120, maxfreq_120_180, maxfreq_180_300

# Short-term features name

st_feature_names = ['zcr', 'energy', 'energy_entropy','spectral_centroid','spectral_spread','spectral_entropy',
'spectral_flux','spectral_rolloff','mfcc_1','mfcc_2','mfcc_3','mfcc_4','mfcc_5','mfcc_6','mfcc_7','mfcc_8','mfcc_9',
'mfcc_10','mfcc_11','mfcc_12','mfcc_13','chroma_1','chroma_2','chroma_3','chroma_4','chroma_5','chroma_6','chroma_7',
'chroma_8','chroma_9','chroma_10','chroma_11','chroma_12','chroma_std']

def file_to_vector_array(file_name,
                         n_mels=64,
                         frames=5,
                         n_fft=1024,
                         hop_length=512,
                         power=2.0):
    """
    convert file_name to a vector array.

    file_name : str
        target .wav file

    return : numpy.array( numpy.array( float ) )
        vector array
        * dataset.shape = (dataset_size, feature_vector_length)
    """
    # 01 calculate the number of dimensions
    dims = n_mels * frames

    # 02 generate melspectrogram using librosa
    y, sr = librosa.load(file_name)
    mel_spectrogram = librosa.feature.melspectrogram(y=y,
                                                     sr=sr,
                                                     n_fft=n_fft,
                                                     hop_length=hop_length,
                                                     n_mels=n_mels,
                                                     power=power)

    # 03 convert melspectrogram to log mel energy
    log_mel_spectrogram = 20.0 / power * np.log10(mel_spectrogram + sys.float_info.epsilon)

    # 04 calculate total vector size
    vector_array_size = len(log_mel_spectrogram[0, :]) - frames + 1

    # 05 skip too short clips
    if vector_array_size < 1:
        return np.empty((0, dims))

    # 06 generate feature vectors by concatenating multiframes
    vector_array = np.zeros((vector_array_size, dims))
    for t in range(frames):
        vector_array[:, n_mels * t: n_mels * (t + 1)] = log_mel_spectrogram[:, t: t + vector_array_size].T

    return vector_array

## Main program ##

def extract(file_out, folder_in):
    # Open a csv and write the title
    f = open(file_out, mode = 'w')
    try:
        string_to_write = "Name_of_Song,Type_of_Dance,Count,BPM,maxfreq_30_40,maxfreq_40_80,maxfreq_80_120,maxfreq_120_180,maxfreq_180_300"
        for name in st_feature_names:
            column_name_mean = "mean_" + name
            column_name_std = "std_" + name
            string_to_write = string_to_write + "," + column_name_mean + "," + column_name_std
        for i in range(64*5):
            string_to_write = string_to_write + "," + "new_feature_{}".format(i)

        f.write(string_to_write + "\n")

        # Open all wav songs and calculate the bpm and write to the csv file
        list_of_dir = os.listdir(folder_in)
        if ".DS_Store" in list_of_dir:
            list_of_dir.remove(".DS_Store")

        for dir in list_of_dir:
            folder_path = folder_in + dir
            file_list = os.listdir(folder_path)
            for file in tqdm(file_list):
                file_path = folder_path + "/" + file
                try:

                    ###### New Features #####
                    new_features = file_to_vector_array(file_path)

                    ###### Count extraction #####
                    proc = madmom.features.DBNDownBeatTrackingProcessor(beats_per_bar=[2, 3, 4], fps=100)
                    act = madmom.features.RNNDownBeatProcessor()(file_path)
                    count = proc(act)
                    max_count = int(count[:,1].max())
                    #print("Successfully extracted count feature")

                    ##### BPM extraction ######
                    data, sr = librosa.load(file_path)
                    tempo, beats = librosa.beat.beat_track(y=data, sr=sr)
                    #print("Successfully extracted bpm feature")

                    ##### Fingerprint extraction ######
                    if data.ndim == 2:
                        data_sum = data[:, 0] + data[:, 1]
                    else:
                        data_sum = data
                    fft_out = fft(data_sum)
                    fft_out = np.abs(fft_out)
                    max_ranges = []
                    for i in range(len(RANGE) - 1):
                        start = i
                        end = start + 1
                        start_freq = RANGE[start]
                        end_freq = RANGE[end]
                        max_range = start_freq + np.argmax(fft_out[start_freq:end_freq])
                        max_ranges.append(max_range)

                    ##### Short-term features and statistic of it (Mid-term) ######
                    # Extract short-term features
                    F, f_names = audioFeatureExtraction.stFeatureExtraction(data, sr, 0.050 * sr, 0.025 * sr)
                    # Extract the stats of short-term features
                    mean_st_features = F.mean(axis=1).reshape(-1)
                    std_st_features = F.std(axis=1).reshape(-1)

                    ##### Write to file #######

                    string_to_write = '"'+ file + '"' + "," + dir + "," + str(max_count) + "," + str(tempo) + \
                                      "," + str(max_ranges[0]) + "," + str(max_ranges[1]) + "," + \
                                      str(max_ranges[2]) + "," + str(max_ranges[3]) + "," + str(max_ranges[4])

                    for index, name in enumerate(f_names):
                        string_to_write = string_to_write + \
                                          "," + str(mean_st_features[index]) + \
                                          "," + str(std_st_features[index])

                    for features in new_features:
                        for feature in features:
                            string_to_write = string_to_write + "," + str(feature)

                    f.write(string_to_write + "\n")

                except Exception as e:
                    print("error in file:",file, "\n" + str(e))
    except:
        f.close()

    finally:
        f.close()


extract(file_out='song_features_public_train.csv', folder_in="../public_chunks_train/")
extract(file_out='song_features_public_test.csv', folder_in="../public_chunks_test/")