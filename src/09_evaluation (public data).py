import numpy as np
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import Dropout
from keras.utils import np_utils
import pandas as pd
from sklearn import metrics
from sklearn.model_selection import train_test_split
import seaborn as sns
import matplotlib.pyplot as plt
from sklearn.preprocessing import StandardScaler
from keras.callbacks import EarlyStopping
import pickle
from keras.optimizers import Adam
from keras.models import load_model

# fix random seed for reproducibility
seed = 7
np.random.seed(seed)

# read the song_features file
fn_train = "song_features_public_train.csv"
df_train = pd.read_csv(fn_train)
# read the song_features file
fn_test = "song_features_public_test.csv"
df_test = pd.read_csv(fn_test)

# subset the features data
features = list(df_train.columns[2:])
df_test["prediction"] = ""

#1st step: Sepaprate Waltz(s) and others:

df_test["Is_Waltz"] = df_test["Count"] == 3
df_test["Is_Not_Waltz"] = df_test["Count"] != 3


#2nd step: Classify Waltz vs Viennese Waltz

df_test_waltz = df_test[df_test["Is_Waltz"]]
# convert types of dance into unique integer numbers in order to classify
targets_waltz = ["Viennesewaltz","Waltz"]
map_to_integer_waltz = {dance:n for n, dance in enumerate(targets_waltz)}
map_to_labels_waltz = {n:dance for n, dance in enumerate(targets_waltz)}
targets_column_test_waltz = df_test_waltz["Type of Dance"].replace(map_to_integer_waltz)

# load model for waltz prediction
model_waltz = load_model("../saved_models/neural_network_waltz.h5")
X_test_waltz = df_test_waltz[features]
y_test_waltz = targets_column_test_waltz

# load the scaler
with open("../saved_models/x_scaler_waltz.pickle", 'rb') as f:
    scaler = pickle.load(f)

X_test_waltz = scaler.transform(X_test_waltz)

# make nn results for Waltz
y_pred_waltz_nn = model_waltz.predict(X_test_waltz)

# Convert y_pred from One-hot to integer
y_pred_waltz_labels = [map_to_labels_waltz[i]for i in y_pred_waltz_nn.argmax(axis = 1)]
df_test_waltz["prediction"] = y_pred_waltz_labels

#3rd step: Classify 7 others

df_test_others = df_test[df_test["Is_Not_Waltz"]]
# convert types of dance into unique integer numbers in order to classify
targets_others = ["Chacha", "Foxtrot", "Jive","Quickstep","Rumba","Samba","Tango"]
map_to_integer_others = {dance:n for n, dance in enumerate(targets_others)}
map_to_labels_others = {n:dance for n, dance in enumerate(targets_others)}
targets_column_test_others = df_test_others["Type of Dance"].replace(map_to_integer_others)

# load model for waltz prediction
model_others = load_model("../saved_models/neural_network.h5")
X_test_others = df_test_others[features]
y_test_others = targets_column_test_others

# load the scaler
with open("../saved_models/x_scaler.pickle", 'rb') as f:
    scaler_others = pickle.load(f)

X_test_others = scaler.transform(X_test_others)

# make nn results for Others
y_pred_others_nn = model_others.predict(X_test_others)

# Convert y_pred from One-hot to integer
y_pred_others_labels = [map_to_labels_others[i]for i in y_pred_others_nn.argmax(axis = 1)]
df_test_others["prediction"] = y_pred_others_labels

#sum(df_test["prediction"]  == df_test["Type of Dance"])